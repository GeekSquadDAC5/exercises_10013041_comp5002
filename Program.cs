﻿using System;

namespace _15th_mar
{
    class Program
    {
        static void Main(string[] args)
        {
/*
            // --------------------------------------------
            // Exercise #13 - Creating a simple app with a single variable
            // --------------------------------------------
            // Start the program with Clear();
            Console.Clear();
            
            
            // Print out my name
            var myName = "Sing Graajae Cho (Shinkee Cho)";
            Console.WriteLine("Welcome to NewShop\n");
            Console.WriteLine($"My name is {myName}.\n");
            
            // --------------------------------------------
            // Exercise #14 - Get a value from a user
            // --------------------------------------------
            
            // Set a variable needed.
            var userBirthMonth = "";
            var userAnswerOfBirthMonth ="";
            Boolean isFished = false;

            // Get user's birthday
            Console.WriteLine("-----------------------------------");
            Console.WriteLine(" Let me ask you some information.");
            Console.WriteLine("-----------------------------------");

            while(!isFished)
            {
                Console.Write("What month are you born in (ex:March or 03)?  : ");
                userBirthMonth = Console.ReadLine();
                Console.WriteLine($"\nYour birth month is {userBirthMonth}\n");

                // --------------------------------------------
                // Exercise #17 - Creating a simple app with a single variable
                // --------------------------------------------
                Console.WriteLine("Is your birth month({userBirthMonth}) correct (Yes/No)?");
                userAnswerOfBirthMonth = Console.ReadLine().ToLower();
                if(userAnswerOfBirthMonth == "yes")
                {
                    Console.WriteLine("Thank you!\n\n");
                    isFished = true;
                }
                else
                {
                    Console.WriteLine("Please type in your birth month again");
                }
            }


            // --------------------------------------------
            // Exercise #15 - Multiplication Tables
            // --------------------------------------------

            isFished = false;       // init this value to false

            // Set a variable needed.
            var firstNum = 0;
            var secondNum = 0;
            Boolean isRepeated = false;

            // Get user's birthday
            Console.WriteLine("-----------------------------------");
            Console.WriteLine(" Let's play multiplication table");
            Console.WriteLine("-----------------------------------");
            
            while(!isFished)
            {
                Console.Write("Please type in first number for multiplication table (1~12) : ");
                firstNum = int.Parse(Console.ReadLine());
                Console.Write("Please type in Second number for multiplication table (1~12) : ");
                secondNum = int.Parse(Console.ReadLine());
                Console.Write($"\nResult : {firstNum} x {secondNum} = {firstNum*secondNum}\n\n");

                // --------------------------------------------
                // Exercise #17 - Creating a simple app with a single variable
                // --------------------------------------------
                Console.WriteLine("Do you want to repeat this game(Yes/No)?");
                isRepeated = Console.ReadLine().ToLower() == "yes" ? true : false;

                // Finish this game
                if(!isRepeated)
                {
                    Console.WriteLine("Thank you!\n\n");
                    isFished = true;
                }
                // user want to repeat to play
                else{ }
            }
*/
            // --------------------------------------------
            // Exercise #18
            // --------------------------------------------
/*
            // Task A
            var num1 = 1;
            var num2 = 2;
            var num3 = 3;
            var num4 = 4;
            var num5 = 5;
            Console.WriteLine("Task A-------------------------------\n");
            Console.WriteLine($"The value of num1 = {num1}, num2 = {num2}, num3 = {num3}, num4 = {num4}, num5 = {num5}\n\n");

            // Task B
            var num6 = 6;
            Console.WriteLine("Task B-------------------------------\n");
            Console.WriteLine($"num1 + num2 = {num1 + num2}");
            Console.WriteLine($"num3 + num4 = {num3 + num4}");
            Console.WriteLine($"num5 + num6 = {num5 + num6}\n\n");


            // Task C
            Console.WriteLine("Task C-------------------------------\n");
            Console.WriteLine($"The value of num1 = {num1}");
            num1 = num1 + num2;
            Console.WriteLine("num1 = num1 + num2");
            Console.WriteLine($"The value of num1 = {num1}\n");
            
            Console.WriteLine($"The value of num3 = {num3}");
            num3 = num3 + num4;
            Console.WriteLine("num3 = num3 + num4");
            Console.WriteLine($"The value of num3 = {num3}\n\n");
*/
            // Task D
            /*
            var strTemp = "I am thinking...";
            var strXnumber = strTemp*3;
            Console.WriteLine($"What will be happened if I multiply string by 3 : {strXnumber}");
            */


            // --------------------------------------------
            // Exercise #19
            // --------------------------------------------
            Console.WriteLine($"a) (6+7) * (3-2) = {(6+7) * (3-2)}");
            Console.WriteLine($"b) (6 * 7) + (3 * 2) = {(6 * 7) + (3 * 2)}");
            Console.WriteLine($"c) (6 * 7) + 3 * 2 = {(6 * 7) + 3 * 2}");
            Console.WriteLine($"d) (3 * 2) + 6 * 7 = {(3 * 2) + 6 * 7}");
            Console.WriteLine($"e) (3 * 2) + 7 * 6 / 2 = {(3 * 2) + 7 * 6 / 2}");
            Console.WriteLine($"f) 6 + 7 * 3 - 2 = {6 + 7 * 3 - 2}");
            Console.WriteLine($"g) 3 * 2 + (3 * 2) = {3 * 2 + (3 * 2)}");
            Console.WriteLine($"h) (6 * 7) * 7 + 6 = {(6 * 7) * 7 + 6}");
            Console.WriteLine($"i) (2 * 2) + 2 * 2 = {(2 * 2) + 2 * 2}");
            Console.WriteLine($"j) 3 * 3 + (3 * 3) = {3 * 3 + (3 * 3)}");
            Console.WriteLine($"k) (6/x2 + 7) * 3 + 2 = {(Math.Pow(6,2) + 7) * 3 + 2}");
            Console.WriteLine($"l) (3 * 2) + 3/x2 * 2 = {(3 * 2) + Math.Pow(3,2) * 2}");
            Console.WriteLine($"m) (6 * (7 + 7)) / 6 = {(6 * (7 + 7)) / 6}");
            Console.WriteLine($"n) ((2 + 2) + (2 * 2)) = {((2 + 2) + (2 * 2))}");
            Console.WriteLine($"o) 4 * 4 + (3/x2 * 3/x2) = {4 * 4 + (Math.Pow(3,2) * Math.Pow(3,2))}");

        }
    }
}
